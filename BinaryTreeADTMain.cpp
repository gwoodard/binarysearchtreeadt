/*
George Woodard
11/20/2014
Programming Assignment
Binary Tree Main file
*/




#include "BinaryTreeADT.h"
#include <iostream>

using namespace std;

int main()
{
	BinaryTreeADT bt;
	myType dat;
	for(int i=0; i<9; i++)
	{
		cout<<endl<<"Enter a number: ";
		cin>>dat;
		bt.insert(dat);
	}

	bt.writeToFile("PRE-ORDER TRAVERSAL");
	bt.preOrderTraverse();

	bt.writeToFile("IN-ORDER TRAVERSAL");
	bt.inOrderTraverse();

	bt.writeToFile("POST-ORDER TRAVERSAL");
	bt.postOrderTraverse();

	return 0;
}