/*
George Woodard
11/20/2014
Programming Assignment
Binary Tree Implementation File
*/



#include "BinaryTreeADT.h"
#include <iostream>
#include <string>

using namespace std;


BinaryTreeADT::BinaryTreeADT()
{
	tree = NULL;
	outFile.open("output.txt");

}

BinaryTreeADT::~BinaryTreeADT()
{
	destroy(tree);
	tree = NULL;
	outFile.close();
}
bool BinaryTreeADT::isEmpty()
{
	if(tree == NULL)
		return true;
	else
		return false;
}
nodePtr BinaryTreeADT::getRoot()
{
	return tree;
}

void BinaryTreeADT::insert(const myType &item)
{
	insertAux(tree, item);
}

void BinaryTreeADT::preOrderTraverse()
{
	preOrder(tree);
}


void BinaryTreeADT::inOrderTraverse()
{
	inOrder(tree);
}

void BinaryTreeADT::postOrderTraverse()
{
	postOrder(tree);
}

void BinaryTreeADT::copy(nodePtr tree)
{
	if(tree != NULL)
	{
		insert(tree->data);
		copy(tree -> left);
		copy(tree->right);
	}
	
}

void BinaryTreeADT::destroy(nodePtr tree)
{
	if(tree != NULL)
	{
		destroy(tree -> left);
		destroy(tree->right);
		delete tree;
	}
}

void BinaryTreeADT::insertAux(nodePtr &tree, const myType &item)
{
	if(tree == NULL)
	{
		tree = getNode(item);
		outFile<<endl<<"Node Value inserted: "<< item;
	}
	else if(item < tree->data)
	{
		insertAux(tree->left, item);
		outFile<<endl<<"Left Value inserted: "<< item;
	}
	else
	{
		insertAux(tree->right, item);
		outFile<<endl<<"Right Value inserted: "<< item;
	}
}

nodePtr BinaryTreeADT::getNode(const myType &item)
{
	nodePtr temp = new node;
	temp->data = item;
	temp->left = NULL;
	temp->right = NULL;
	return temp;
}

//This shows how to do transvere of a Binary Tree. For Pre, you process the data, then go left then right.
void BinaryTreeADT::preOrder(nodePtr tree)
{
	if(tree != NULL)
	{
		process(tree->data);
		preOrder(tree->left);
		preOrder(tree->right);
	}
}

//This shows how to do transvere of a Binary Tree. For In, you go left then right, then process the data, then go right.
void BinaryTreeADT::inOrder(nodePtr tree)
{
	if(tree != NULL)
	{
		inOrder(tree->left);
		process(tree->data);
		inOrder(tree->right);
	}
}

//This shows how to do transvere of a Binary Tree. For Post, you go left then right then process the data.
void BinaryTreeADT::postOrder(nodePtr tree)
{
	if(tree != NULL)
	{
		postOrder(tree->left);
		postOrder(tree->right);
		process(tree->data);
	}
}

void BinaryTreeADT::process(const myType &item)
{
	cout<<endl<<item<<endl;
	outFile<<endl<<item<<endl;
}

void BinaryTreeADT::writeToFile(string s)
{
	outFile<<endl<<s;
}