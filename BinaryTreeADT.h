/*
George Woodard
11/20/2014
Programming Assignment
Binary Tree Header File
*/


#include <fstream>
#include <iostream>
#include <string>

using namespace std;

struct node;
typedef node* nodePtr;
typedef int myType;

struct node{

	myType data;
	nodePtr left;
	nodePtr right;

};


class BinaryTreeADT{


public:

	BinaryTreeADT();
	~BinaryTreeADT();
	bool isEmpty();
	nodePtr getRoot();
	void insert (const myType &item);
	void preOrderTraverse();
	void inOrderTraverse();
	void postOrderTraverse();
	void writeToFile(string s);

private:

	nodePtr tree;
	ofstream outFile;
	void copy(nodePtr tree);
	void destroy(nodePtr tree);
	nodePtr getNode(const myType &item);
	void preOrder(nodePtr tree);
	void inOrder(nodePtr tree);
	void postOrder(nodePtr tree);
	void process(const myType &item);
	void insertAux(nodePtr &tree, const myType &item);
};