# Binary Search Tree Abstract Data Type #

### **About:** ###

Binary Search Tree ADT that allows the input of nine integers. Then each integer is sorted and inserted in the a Binary Tree based on the Binary Search Tree algorithm. Finally the numbers are sorted in Pre-Order Transversal, In-Order Transversal, Post-Order Transversal and all wrote to an output file called "output.txt"

### **IDE:**
Visual Studio

### **Input:** ###
Nine Integers

### **Output:** ###
Binary Tree, Pre-Order Transversal, In-Order Transversal, Post-Order Transversal, Output file (output.txt)

### **Language:** ###
C++